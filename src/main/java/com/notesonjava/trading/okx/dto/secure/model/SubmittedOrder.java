package com.notesonjava.trading.okx.dto.secure.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class SubmittedOrder {

	@JsonProperty("ordId")
	private String serverId;
	

	@JsonProperty("clOrdId")
	private String clientId;
	
	@JsonProperty("sCode")
	private int code;
	
	@JsonProperty("sMsg")
	private String message;
	
	private String tag;
}
