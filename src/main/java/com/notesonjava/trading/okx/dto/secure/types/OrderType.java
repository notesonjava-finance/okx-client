package com.notesonjava.trading.okx.dto.secure.types;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum OrderType {
	@JsonProperty("market")
	MARKET,
	@JsonProperty("limit")
	LIMIT,
	@JsonProperty("post_only")
	POST_ONLY,
	@JsonProperty("fok")
	FILL_OR_KILL,
	@JsonProperty("ioc")
	IMMEDIATE_OR_CANCEL,
	@JsonProperty("optimal_limit_ioc")
	OPTIMAL_LIMIT_IOC;

}
