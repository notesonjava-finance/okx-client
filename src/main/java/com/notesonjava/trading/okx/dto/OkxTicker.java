package com.notesonjava.trading.okx.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class OkxTicker {
	
	private String instType;
    private String instId;
    private double last;
    private double askPx;
    private double bidPx;
    private long ts;

}
