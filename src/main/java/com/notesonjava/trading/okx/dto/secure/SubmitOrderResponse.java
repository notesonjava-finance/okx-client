package com.notesonjava.trading.okx.dto.secure;

import java.util.Collections;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.notesonjava.trading.okx.dto.secure.model.SubmittedOrder;

import lombok.Data;

@Data
public class SubmitOrderResponse {
	
	private int code;
	private List<SubmittedOrder> data;
	@JsonProperty("msg")
	private String message;
	
	public List<SubmittedOrder> getData() {
		return Collections.unmodifiableList(data);
	}
	
	
}
