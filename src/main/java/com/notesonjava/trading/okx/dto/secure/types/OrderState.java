package com.notesonjava.trading.okx.dto.secure.types;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum OrderState {
	@JsonProperty("live")
    LIVE,
    @JsonProperty("partially_filled")
	PARTIALLY_FILLED,
	@JsonProperty("filled")
	FILLED,
	@JsonProperty("canceled")
	CANCELED;

}
