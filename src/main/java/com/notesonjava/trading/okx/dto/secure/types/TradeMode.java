package com.notesonjava.trading.okx.dto.secure.types;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum TradeMode {

	@JsonProperty("isolated")
	ISOLATED,
	
	@JsonProperty("cross")
	CROSS,
	
	@JsonProperty("cash")
	CASH;
	
}
