package com.notesonjava.trading.okx.dto.secure;

import java.util.Collections;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.notesonjava.trading.okx.dto.secure.model.OrderDetails;

import lombok.Data;

@Data
public class OrderDetailsResponse {
	private int code;
	@JsonProperty("msg")
	private String message;
	private List<OrderDetails> data;
	
	public List<OrderDetails> getData() {
		return Collections.unmodifiableList(data);
	}
	
	
	
}

