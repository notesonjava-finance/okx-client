package com.notesonjava.trading.okx.dto;

import java.util.Collections;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class StatusResponse {

	private int code;
	@JsonProperty("msg")
	private String message;
	private List<StatusData> data;
	
	public List<StatusData> getData() {
		return Collections.unmodifiableList(data);
	}
	
}
