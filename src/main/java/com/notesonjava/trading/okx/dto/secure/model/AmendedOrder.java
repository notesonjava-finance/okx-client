package com.notesonjava.trading.okx.dto.secure.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class AmendedOrder {

	@JsonProperty("clOrdId")
	private String clientId;
	@JsonProperty("ordId")
    private String serverId;
	@JsonProperty("reqId")
	private String requestId;
	@JsonProperty("sCode")
	private int code;
	@JsonProperty("sMsg")
    private String message;
}
