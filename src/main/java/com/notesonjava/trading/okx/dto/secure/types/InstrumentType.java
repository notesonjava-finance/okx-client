package com.notesonjava.trading.okx.dto.secure.types;

public enum InstrumentType {
	SPOT,
	MARGIN,
	SWAP,
	FUTURES,
	OPTION;
}
