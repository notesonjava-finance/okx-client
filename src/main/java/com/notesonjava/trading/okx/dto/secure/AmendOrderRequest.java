package com.notesonjava.trading.okx.dto.secure;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
@JsonInclude(Include.NON_DEFAULT)
public class AmendOrderRequest {

	@JsonProperty("instId")
	private String symbol;
	@JsonProperty("ordId")
	private String serverId;
	@JsonProperty("clOrdId")
	private String clientId;
	
	@JsonProperty("newSz")
	private double newSize;
	@JsonProperty("newPx")
	private double newPrice;
	
}
