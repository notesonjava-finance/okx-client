package com.notesonjava.trading.okx.dto;

import java.util.Collections;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class TickerResponse {

	private int code;
	@JsonProperty("msg")
	private String message;
    private List<OkxTicker> data;
    
    public List<OkxTicker> getData() {
		return Collections.unmodifiableList(data);
	}
	
}
