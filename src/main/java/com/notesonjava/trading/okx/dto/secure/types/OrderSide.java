package com.notesonjava.trading.okx.dto.secure.types;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum OrderSide {

	@JsonProperty("buy")
	BUY,
	@JsonProperty("sell")
	SELL;
	
}
