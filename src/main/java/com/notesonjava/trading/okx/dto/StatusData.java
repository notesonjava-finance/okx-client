package com.notesonjava.trading.okx.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class StatusData {
	
	private long begin;
    private long end;
    private String href;
    private long preOpenBegin;
    @JsonProperty("scheDesc")
    private String rescheduledDescription;
    private int serviceType;
    private String state;
    private String system;
    private String title;
    private String maintType;
    private String env;

}
