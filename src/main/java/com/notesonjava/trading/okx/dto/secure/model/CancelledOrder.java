package com.notesonjava.trading.okx.dto.secure.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class CancelledOrder {

	@JsonProperty("clOrdId")
	private String clientId;
	@JsonProperty("ordId")
    private String serverId;
	@JsonProperty("sCode")
    private int code;
	@JsonProperty("sMsg")
    private String message;

}
