package com.notesonjava.trading.okx.dto.secure;

import java.util.Collections;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.notesonjava.trading.okx.dto.secure.model.AmendedOrder;

import lombok.Data;

@Data
public class AmendOrderResponse {

	private int code;
	@JsonProperty("msg")
	private String message;
	private List<AmendedOrder> data;

	public List<AmendedOrder> getData() {
		return Collections.unmodifiableList(data);
	}

}
