package com.notesonjava.trading.okx.dto.secure.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class Balance {

	@JsonProperty("adjEq")
	private double adjustedEquity;
    private List<BalanceDetails> details; 
    @JsonProperty("imr")
    private double initialMarginRequirement;
    @JsonProperty("isoEq")
    private int isolatedEquity;
    @JsonProperty("mgnRatio")
    private double marginRatio;
    @JsonProperty("mmr")
    private double maintenanceMarginRequirement;
    private double notionalUsd;
    @JsonProperty("ordFroz")
    private int orderFrozen;
    @JsonProperty("totalEq")
    private double totalEquity;
    @JsonProperty("uTime")
    private long updateTime;
	
}
