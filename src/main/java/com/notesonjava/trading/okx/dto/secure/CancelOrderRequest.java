package com.notesonjava.trading.okx.dto.secure;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class CancelOrderRequest {
	@JsonProperty("instId")
	private String instrumentId;
	@JsonProperty("ordId")
	private String serverId;
	
}
