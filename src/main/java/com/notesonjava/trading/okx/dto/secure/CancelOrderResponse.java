package com.notesonjava.trading.okx.dto.secure;

import java.util.Collections;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.notesonjava.trading.okx.dto.secure.model.CancelledOrder;

import lombok.Data;

@Data
public class CancelOrderResponse {

	private int code;
	@JsonProperty("msg")
	private String message;
	private List<CancelledOrder> data;
	
	public List<CancelledOrder> getData() {
		return Collections.unmodifiableList(data);
	}
	
}
