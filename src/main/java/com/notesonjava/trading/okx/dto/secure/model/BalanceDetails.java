package com.notesonjava.trading.okx.dto.secure.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class BalanceDetails {
    @JsonProperty("availBal")
    private String availableBalance;

    @JsonProperty("availEq")
    private String availableEquity;

    @JsonProperty("cashBal")
    private String cashBalance;

    @JsonProperty("ccy")
    private String currency;

    @JsonProperty("crossLiab")
    private String crossLiability;

    @JsonProperty("disEq")
    private String discountEquity;

    @JsonProperty("eq")
    private String equity;

    @JsonProperty("eqUsd")
    private String eqUsd;

    @JsonProperty("frozenBal")
    private String frozenBalance;

    @JsonProperty("interest")
    private String interest;

    @JsonProperty("isoEq")
    private String isolatedEquity;

    @JsonProperty("isoLiab")
    private String isolatedLiability;

    @JsonProperty("isoUpl")
    private String isolatedUnrealizedProfitLoss;

    @JsonProperty("liab")
    private String liabilities;

    @JsonProperty("maxLoan")
    private String maxLoan;

    @JsonProperty("mgnRatio")
    private String marginRatio;

    @JsonProperty("notionalLever")
    private String notionalLeverage;

    @JsonProperty("ordFrozen")
    private String ordersFrozen;

    @JsonProperty("twap")
    private String twap;

    @JsonProperty("uTime")
    private String updateTime;

    @JsonProperty("upl")
    private String unrealizedProfitLoss;

    @JsonProperty("uplLiab")
    private String unrealizedProfitLossLiability;

    @JsonProperty("stgyEq")
    private String strategyEquity;

    @JsonProperty("spotInUseAmt")
    private String spotInUseAmount;
}
