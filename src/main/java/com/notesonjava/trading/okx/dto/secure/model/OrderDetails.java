package com.notesonjava.trading.okx.dto.secure.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.notesonjava.trading.okx.dto.secure.types.InstrumentType;
import com.notesonjava.trading.okx.dto.secure.types.OrderSide;
import com.notesonjava.trading.okx.dto.secure.types.OrderState;
import com.notesonjava.trading.okx.dto.secure.types.OrderType;
import com.notesonjava.trading.okx.dto.secure.types.TradeMode;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class OrderDetails {

	@JsonProperty("accFillSz")
	private double accumulatedFill;
	@JsonProperty("avgPx")
	private double averagePrice;
	@JsonProperty("cTime")
	private long creationTime;
	
	private double fee;
	@JsonProperty("feeCcy")
	private String feeCurrency;
	@JsonProperty("fillPx")
	private double fillPrice;
	@JsonProperty("fillSz")
	private double fillSize;
	
	private long fillTime;
	@JsonProperty("instId")
	private String instrumentId;
	@JsonProperty("instType")
	private InstrumentType instrumentType;
	
	@JsonProperty("ordId")
	private String serverId;
	@JsonProperty("ordType")
	private OrderType orderType;
	@JsonProperty("posSide")
	private String positionSide;
	@JsonProperty("px")
	private double price;
	private String rebate;
	@JsonProperty("rebateCcy")
	private String rebateCurrency;
	private OrderSide side;
	private OrderState state;
	@JsonProperty("sz")
	private double size;
	private String tag;
	@JsonProperty("tgtCcy")
	private String targetCurrency;
	@JsonProperty("tdMode")
	private TradeMode tradeMode;
	private String source;
	private String tradeId;
	@JsonProperty("uTime")
	private long updateTime;
	@JsonProperty("clOrdId")
	private String clientId;

}
