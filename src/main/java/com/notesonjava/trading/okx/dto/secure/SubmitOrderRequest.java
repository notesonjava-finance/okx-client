package com.notesonjava.trading.okx.dto.secure;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.notesonjava.trading.okx.dto.secure.types.OrderSide;
import com.notesonjava.trading.okx.dto.secure.types.OrderType;
import com.notesonjava.trading.okx.dto.secure.types.TradeMode;

import lombok.Data;

@Data
public class SubmitOrderRequest {
	
	@JsonProperty("instId")
	private String instrumentId;
	
	@JsonProperty("tdMode")
	private TradeMode tradeMode;
	 
	private OrderSide side;
	    
	@JsonProperty("ordType")
	private OrderType orderType;
	   
	@JsonProperty("px")
	private double price;
	  
	@JsonProperty("sz")
	private double size;
	
	@JsonProperty("clOrdId")
	private String clientId;
	

}
