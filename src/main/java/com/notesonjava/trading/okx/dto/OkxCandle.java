package com.notesonjava.trading.okx.dto;

import lombok.Data;

@Data
public class OkxCandle {
    private long openTime;
    private double open;
    private double high;
    private double low;
    private double close;
    private double volume;
    private long closeTime;
    private boolean completed;
}
