package com.notesonjava.trading.okx.dto;

public enum CandleInterval {

	MINUTE_1("1m"), 
	MINUTE_3("3m"), 
	MINUTE_5("5m"), 
	MINUTE_15("15m"), 
	MINUTE_30("30m"), 
	HOUR_1("1H"), 
	HOUR_2("2H"),
	HOUR_4("4H"), 
	HOUR_6("6H"), 
	HOUR_12("12H"), 
	DAY_1("1D"), 
	DAY_2("2D"), 
	DAY_3("3D"), 
	WEEK_1("1W"), 
	MONTH_1("1M"),
	MONTH_3("3M");

	private String code;

	CandleInterval(String code) {
		this.code = code;
	}

	public String getCode() {
		return this.code;
	}

	public static CandleInterval fromCode(String code) {
		for (CandleInterval e : CandleInterval.values()) {
			if (e.code.equals(code)) {
				return e;
			}
		}
		throw new IllegalArgumentException("Invalid code: " + code);
	}
}
