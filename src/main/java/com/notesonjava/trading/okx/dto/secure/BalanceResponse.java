package com.notesonjava.trading.okx.dto.secure;

import java.util.Collections;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.notesonjava.trading.okx.dto.secure.model.Balance;

import lombok.Data;

@Data
public class BalanceResponse {

	private int code;
	@JsonProperty("msg")
	private String message;
	private List<Balance> data;

	public List<Balance> getData() {
		return Collections.unmodifiableList(data);
	}

}
