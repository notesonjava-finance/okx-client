package com.notesonjava.trading.okx;

public class UrlBuilder {

	static final String BALANCE_URL = "/api/v5/account/balance";
	static final String ORDER_HISTORY_URL = "/api/v5/trade/orders-history";
	static final String ORDER_DETAIL_URL = "/api/v5/trade/order";
	
	

	
	public static String getBalances(String...currencies) {
		StringBuilder sb = new StringBuilder(BALANCE_URL);
		if(currencies.length > 0) {
			sb.append("?");
			sb.append("ccy=");
			for(int i = 0; i < currencies.length; i ++) {
				sb.append(currencies[i]);
				if(i < currencies.length - 1) {
					sb.append(",");
				}
			}
		}
		return sb.toString();
		
	}
	
	public static String getOrderHistory(String symbol) {
		return ORDER_HISTORY_URL + "?instType=SPOT&instId=" + symbol;	
	}
	
	public static String getOrderHistory(String symbol, long fromTimestamp) {
		return ORDER_HISTORY_URL + "?instType=SPOT&instId=" + symbol + "&begin=" + fromTimestamp;	
	}
	public static String getOrderDetails(String orderId, String instrumentId) {
		return ORDER_DETAIL_URL + "?ordId=" + orderId + "&instId=" + instrumentId;	
	}
	
}
