package com.notesonjava.trading.okx;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.notesonjava.trading.okx.dto.OkxCandle;
import com.notesonjava.trading.okx.serialization.OkxCandleDeserializer;

public class OkxClientFactory {
	
	public static ObjectMapper createObjectMapper() {
		
		ObjectMapper mapper = new ObjectMapper();
		mapper.registerModule(createModule());
		return mapper;
	}
	
	public static SimpleModule createModule() {
		SimpleModule module = new SimpleModule();
		module.addDeserializer(OkxCandle.class, new OkxCandleDeserializer());
		return module;
	}
	
	public static OkxSecureClient createSecureClient(String secretKey, String apiKey, String passPhrase) {

		SignatureHelper sigHelper = new SignatureHelper(secretKey);
		HttpHeaderHelper httpHelper = new HttpHeaderHelper(apiKey, passPhrase, sigHelper, false);
		
		OrderRequestFactory orderFactory = new OrderRequestFactory();
		ObjectMapper mapper = new ObjectMapper();
		SecureRequestProcessor processor = new SecureRequestProcessor("https://www.okx.com", httpHelper, mapper);
		return new OkxSecureClient(new ObjectMapper(), orderFactory, processor);
	}

	public static OkxSecureClient createDemoClient(String secretKey, String apiKey, String passPhrase) {

		SignatureHelper sigHelper = new SignatureHelper(secretKey);
		HttpHeaderHelper httpHelper = new HttpHeaderHelper(apiKey, passPhrase, sigHelper, true);
		OrderRequestFactory orderFactory = new OrderRequestFactory();
		ObjectMapper mapper = new ObjectMapper();
		SecureRequestProcessor processor = new SecureRequestProcessor("https://www.okx.com", httpHelper, mapper);
		return new OkxSecureClient(new ObjectMapper(), orderFactory, processor);
	}

	
	public static OkxPublicClient createPublicClient() {
		ObjectMapper okxMapper = new ObjectMapper();
		OkxCandleDeserializer d2 = new OkxCandleDeserializer();
		okxMapper.registerModule(new SimpleModule().addDeserializer(OkxCandle.class, d2));
		return new OkxPublicClient("https://www.okx.com", okxMapper);
	}
}
