package com.notesonjava.trading.okx;

import java.util.List;
import java.util.stream.Collectors;

import com.notesonjava.trading.okx.dto.secure.AmendOrderRequest;
import com.notesonjava.trading.okx.dto.secure.CancelOrderRequest;
import com.notesonjava.trading.okx.dto.secure.SubmitOrderRequest;
import com.notesonjava.trading.okx.dto.secure.types.OrderSide;
import com.notesonjava.trading.okx.dto.secure.types.OrderType;
import com.notesonjava.trading.okx.dto.secure.types.TradeMode;

public class OrderRequestFactory {
	
	public SubmitOrderRequest createSellOrderRequest(double price, double size, String symbol) {
		SubmitOrderRequest req = new SubmitOrderRequest();
		req.setInstrumentId(symbol);
		req.setTradeMode(TradeMode.CASH);
		req.setSide(OrderSide.SELL);
		req.setOrderType(OrderType.LIMIT);
		req.setPrice(price);
		req.setSize(size);
		return req;
	}

	public SubmitOrderRequest createBuyOrderRequest(double price, double size, String symbol) {
		SubmitOrderRequest req = new SubmitOrderRequest();
		req.setInstrumentId(symbol);
		req.setTradeMode(TradeMode.CASH);
		req.setSide(OrderSide.BUY);
		req.setOrderType(OrderType.LIMIT);
		req.setPrice(price);
		req.setSize(size);
		return req;
	}
	

	public AmendOrderRequest createAmendSizeRequest(String orderId, String symbol, double newSize) {
		AmendOrderRequest req = new AmendOrderRequest();
		req.setServerId(orderId);
		req.setSymbol(symbol);
		req.setNewSize(newSize);
		return req;
	}

	public AmendOrderRequest createAmendPriceRequest(String orderId, String symbol, double newPrice) {
		AmendOrderRequest req = new AmendOrderRequest();
		req.setServerId(orderId);
		req.setSymbol(symbol);
		req.setNewPrice(newPrice);
		return req;
	}

	
	public CancelOrderRequest createCancelRequest(String orderId, String symbol) {
		CancelOrderRequest request = new CancelOrderRequest();
		request.setInstrumentId(symbol);
		request.setServerId(orderId);
		
		return request;
	}

	public List<CancelOrderRequest> createCancelOrdersRequest(List<String> orderIds, String symbol) {
		List<CancelOrderRequest> request = orderIds.stream().map(id -> {
			CancelOrderRequest cor = new CancelOrderRequest();
			cor.setInstrumentId(symbol);
			cor.setServerId(id);
			return cor;
		}).collect(Collectors.toList());
		return request;
	}
	
	public AmendOrderRequest createAmendOrderRequest(String orderId, String symbol, double newPrice, double newSize) {
		AmendOrderRequest req = new AmendOrderRequest();
		req.setServerId(orderId);
		req.setSymbol(symbol);
		req.setNewPrice(newPrice);
		req.setNewSize(newSize);
		return req;
	}
}
