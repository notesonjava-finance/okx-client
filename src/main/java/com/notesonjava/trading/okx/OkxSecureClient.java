package com.notesonjava.trading.okx;

import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.notesonjava.trading.okx.dto.secure.AmendOrderRequest;
import com.notesonjava.trading.okx.dto.secure.AmendOrderResponse;
import com.notesonjava.trading.okx.dto.secure.BalanceResponse;
import com.notesonjava.trading.okx.dto.secure.CancelOrderRequest;
import com.notesonjava.trading.okx.dto.secure.CancelOrderResponse;
import com.notesonjava.trading.okx.dto.secure.OrderDetailsResponse;
import com.notesonjava.trading.okx.dto.secure.OrderHistoryResponse;
import com.notesonjava.trading.okx.dto.secure.OrderListResponse;
import com.notesonjava.trading.okx.dto.secure.SubmitOrderRequest;
import com.notesonjava.trading.okx.dto.secure.SubmitOrderResponse;
import com.notesonjava.trading.okx.exceptions.OkxCommunicationException;
import com.notesonjava.trading.okx.exceptions.OkxRequestException;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@AllArgsConstructor
public class OkxSecureClient {
	static final String ORDER_LIST_URL = "/api/v5/trade/orders-pending";
	static final String CANCEL_URL = "/api/v5/trade/cancel-order";
	static final String MULTI_CANCEL_URL = "/api/v5/trade/cancel-batch-orders";
	static final String PLACE_ORDER_URL = "/api/v5/trade/order";
	static final String MULTI_ORDER_URL = "/api/v5/trade/batch-orders";
	static final String AMEND_URL = "/api/v5/trade/amend-order";
	static final String MULTI_AMEND_URL = "/api/v5/trade/amend-batch-orders";

	
	
	

	private ObjectMapper mapper;
	private OrderRequestFactory factory;

	private SecureRequestProcessor processor;


	public OrderListResponse getActiveOrders() throws OkxCommunicationException, OkxRequestException {
		return processor.processGet(ORDER_LIST_URL, OrderListResponse.class);
	}

	public BalanceResponse getBalances(String...currencies) throws OkxCommunicationException, OkxRequestException {
		return processor.processGet(UrlBuilder.getBalances(currencies), BalanceResponse.class);
	}

	
	public CancelOrderResponse cancelOrder(String orderId, String symbol) throws OkxCommunicationException, OkxRequestException  {

		CancelOrderRequest request = factory.createCancelRequest(orderId, symbol);
		String json;
		try {
			json = mapper.writeValueAsString(request);
		} catch (JsonProcessingException e) {
			log.error("Unable to parse : " + request);
			throw new OkxRequestException("Unable to create json request", e);
		}
		return processor.processPost(CANCEL_URL, json, CancelOrderResponse.class);
	}

	public CancelOrderResponse cancelOrders(List<String> orderIds, String symbol) throws OkxCommunicationException, OkxRequestException  {
		if(orderIds.size() > 20) {
			throw new OkxRequestException("Cannot cancel more than 20 orders in a single call");
		}

		List<CancelOrderRequest> request = factory.createCancelOrdersRequest(orderIds, symbol);
		String json;
		try {
			json = mapper.writeValueAsString(request);
		} catch (JsonProcessingException e) {
			log.error("Unable to parse : " + request);
			throw new OkxRequestException("Unable to create json request", e);
		}
		return processor.processPost(MULTI_CANCEL_URL, json, CancelOrderResponse.class);

	}

	public SubmitOrderResponse submitBuyOrder(double price, double quantity, String symbol) throws OkxCommunicationException, OkxRequestException  {
		SubmitOrderRequest req = factory.createBuyOrderRequest(price, quantity, symbol);
		String json;
		try {
			json = mapper.writeValueAsString(req);
		} catch (JsonProcessingException e) {
			log.error("Unable to parse : " + req);
			throw new OkxRequestException("Unable to create json request", e);
		}
		return processor.processPost(PLACE_ORDER_URL, json, SubmitOrderResponse.class);

	}
	
	public SubmitOrderResponse submitOrder(SubmitOrderRequest req) throws OkxCommunicationException, OkxRequestException  {
		String json;
		try {
			json = mapper.writeValueAsString(req);
		} catch (JsonProcessingException e) {
			log.error("Unable to parse : " + req);
			throw new OkxRequestException("Unable to create json request", e);
		}
		return processor.processPost(PLACE_ORDER_URL, json, SubmitOrderResponse.class);

	}

	public SubmitOrderResponse submitOrders(List<SubmitOrderRequest> orders) throws OkxCommunicationException, OkxRequestException  {
		if(orders.size() > 20) {
			throw new OkxRequestException("Cannot submit more than 20 orders in a single call");
		}

		String json;
		try {
			json = mapper.writeValueAsString(orders);
		} catch (JsonProcessingException e) {
			log.error("Unable to parse : " + orders);
			throw new OkxRequestException("Unable to create json request", e);
		}
		return processor.processPost(MULTI_ORDER_URL, json, SubmitOrderResponse.class);

	}

	public SubmitOrderResponse submitSellOrder(double price, double quantity, String symbol) throws OkxCommunicationException, OkxRequestException {
		SubmitOrderRequest req = factory.createSellOrderRequest(price, quantity, symbol);
		String json;
		try {
			json = mapper.writeValueAsString(req);
		} catch (JsonProcessingException e) {
			throw new OkxRequestException("Unable to create json request", e);
		}
		return processor.processPost(PLACE_ORDER_URL, json, SubmitOrderResponse.class);
	}

	public OrderDetailsResponse getOrderDetails(String orderId, String intrumentId) throws OkxCommunicationException, OkxRequestException {
		String requestUrl = UrlBuilder.getOrderDetails(orderId, intrumentId);
		return processor.processGet(requestUrl, OrderDetailsResponse.class);
	}

	public AmendOrderResponse amendOrder(String orderId, String symbol, double newPrice, double newSize) throws OkxCommunicationException, OkxRequestException {

		AmendOrderRequest req = factory.createAmendOrderRequest(orderId, symbol, newPrice, newSize);

		String json;
		try {
			json = mapper.writeValueAsString(req);
		} catch (JsonProcessingException e) {
			throw new OkxRequestException("Unable to create json request", e);
		}
		return processor.processPost(AMEND_URL, json, AmendOrderResponse.class);
	}

	

	public AmendOrderResponse amendOrderPrice(String orderId, String symbol, double newPrice) throws OkxCommunicationException, OkxRequestException {

		AmendOrderRequest req = factory.createAmendPriceRequest(orderId, symbol, newPrice);
		String json;
		try {
			json = mapper.writeValueAsString(req);
		} catch (JsonProcessingException e) {
			throw new OkxRequestException("Unable to create json body", e);
		}
		return processor.processPost(AMEND_URL, json, AmendOrderResponse.class);
	}

	public AmendOrderResponse amendOrderSize(String orderId, String symbol, double newSize) throws OkxCommunicationException, OkxRequestException {

		AmendOrderRequest req = factory.createAmendSizeRequest(orderId, symbol, newSize);
		String json;
		try {
			json = mapper.writeValueAsString(req);
		} catch (JsonProcessingException e) {
			throw new OkxRequestException("Unable to create json body", e);
		}
		return processor.processPost(AMEND_URL, json, AmendOrderResponse.class);

	}

	public AmendOrderResponse amendOrder(AmendOrderRequest order) throws OkxCommunicationException, OkxRequestException  {
		String json;
		try {
			json = mapper.writeValueAsString(order);
		} catch (JsonProcessingException e) {
			throw new OkxRequestException("Unable to convert orders into json", e);
		}
		return processor.processPost(AMEND_URL, json, AmendOrderResponse.class);
	}

	
	public AmendOrderResponse amendOrders(List<AmendOrderRequest> orders) throws OkxCommunicationException, OkxRequestException  {
		if(orders.size() > 20) {
			throw new OkxRequestException("Cannot amend more than 20 orders in a single call");
		}

		String json;
		try {
			json = mapper.writeValueAsString(orders);
		} catch (JsonProcessingException e) {
			throw new OkxRequestException("Unable to convert orders into json", e);
		}
		return processor.processPost(MULTI_AMEND_URL, json, AmendOrderResponse.class);
	}

	public CancelOrderResponse cancelOrders(List<CancelOrderRequest> orders) throws OkxCommunicationException, OkxRequestException {
		if(orders.size() > 20) {
			throw new OkxRequestException("Cannot cancel more than 20 orders in a single call");
		}

		String json;
		try {
			json = mapper.writeValueAsString(orders);
		} catch (JsonProcessingException e) {
			throw new OkxRequestException("Unable to convert orders into json", e);
		}
		return processor.processPost(MULTI_CANCEL_URL, json, CancelOrderResponse.class);
	}

	public OrderHistoryResponse getOrderHistory(String symbol) throws OkxCommunicationException, OkxRequestException {
		String url = UrlBuilder.getOrderHistory(symbol);
		return processor.processGet(url, OrderHistoryResponse.class);
	}
	
	public OrderHistoryResponse getOrderHistory(String symbol, long fromTimestamp) throws OkxCommunicationException, OkxRequestException {
		String url =  UrlBuilder.getOrderHistory(symbol, fromTimestamp);
		return processor.processGet(url, OrderHistoryResponse.class);
	} 
	

}
