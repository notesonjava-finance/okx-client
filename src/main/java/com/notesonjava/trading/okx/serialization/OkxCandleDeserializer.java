package com.notesonjava.trading.okx.serialization;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.notesonjava.trading.okx.dto.OkxCandle;

public class OkxCandleDeserializer extends JsonDeserializer<OkxCandle> {

    @Override
    public OkxCandle deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        JsonNode node = jsonParser.getCodec().readTree(jsonParser);
        OkxCandle candleDto = new OkxCandle();
        candleDto.setOpenTime(node.get(0).asLong());
        candleDto.setOpen(node.get(1).asDouble());
        candleDto.setHigh(node.get(2).asDouble());
        candleDto.setLow(node.get(3).asDouble());
        candleDto.setClose(node.get(4).asDouble());
        candleDto.setVolume(node.get(5).asDouble());
        int completed = node.get(8).asInt();
        candleDto.setCompleted(completed == 1?true: false);
        return candleDto;
    }
}