package com.notesonjava.trading.okx;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@AllArgsConstructor
class SignatureHelper {
	
	private String SECRET_KEY;
	

	public String createSignature(String timestamp, String method, String requestPath) {
		String message = createMessageToSign(timestamp, method, requestPath);
		return encrypt(message);
	}
	
	public String createSignature(String timestamp, String method, String requestPath, String body) {
		String message = createMessageToSign(timestamp, method, requestPath, body);
		return encrypt(message);
	}
	
	
	private String createMessageToSign(String timestamp, String method, String requestPath) {
		StringBuilder sb = new StringBuilder();
		sb.append(timestamp);
		sb.append(method);
		sb.append(requestPath);
	    return sb.toString();
		
	}
	
	private String createMessageToSign(String timestamp, String method, String requestPath, String body) {
		StringBuilder sb = new StringBuilder();
		sb.append(timestamp);
		sb.append(method);
		sb.append(requestPath);
		sb.append(body);
		return sb.toString();
		
	}

	private String encrypt(String message) {
		try {
			Mac mac = Mac.getInstance("HmacSHA256");
			SecretKeySpec secret_key = new SecretKeySpec(SECRET_KEY.getBytes(), "HmacSHA256");
			mac.init(secret_key);
			return encode(mac.doFinal(message.getBytes()));
		} catch (NoSuchAlgorithmException | InvalidKeyException e) {
			log.error("Encryption error", e);
			return null;
		}
	}

	private String encode(byte[] input) {
		return Base64.getEncoder().encodeToString(input);
	}
	
}
