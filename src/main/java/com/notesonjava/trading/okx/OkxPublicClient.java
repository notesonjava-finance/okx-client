package com.notesonjava.trading.okx;

import java.io.IOException;

import com.fasterxml.jackson.core.exc.StreamReadException;
import com.fasterxml.jackson.databind.DatabindException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.notesonjava.trading.okx.dto.CandleInterval;
import com.notesonjava.trading.okx.dto.CandleResponse;
import com.notesonjava.trading.okx.dto.StatusResponse;
import com.notesonjava.trading.okx.dto.TickerResponse;
import com.notesonjava.trading.okx.exceptions.OkxCommunicationException;
import com.notesonjava.trading.okx.exceptions.OkxRequestException;

import jodd.http.HttpRequest;
import jodd.http.HttpResponse;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@AllArgsConstructor
public class OkxPublicClient {

	private String baseUrl;
	private ObjectMapper mapper;

	static final String CANDLE_URL = "/api/v5/market/candles";
	static final String TICKER_URL = "/api/v5/market/ticker";
	static final String STATUS_URL = "/api/v5/system/status";

	
	
	public CandleResponse getCandles(String symbol, CandleInterval interval) throws OkxCommunicationException, OkxRequestException {
		return getCandles(symbol, interval, 20);
	}
	
	public CandleResponse getCandles(String symbol, CandleInterval interval, int numPeriods) throws OkxCommunicationException, OkxRequestException {
		HttpRequest request = HttpRequest.get(baseUrl + CANDLE_URL + "?instId="+symbol+"&limit="+numPeriods+"&bar="+interval.getCode());
		return process(request, CandleResponse.class);	
	}
	
	public TickerResponse getTicker(String symbol) throws OkxCommunicationException, OkxRequestException  {
		HttpRequest request = HttpRequest.get(baseUrl + TICKER_URL + "?instId=" + symbol);
		return process(request, TickerResponse.class);
	}
	
	public StatusResponse getStatus() throws StreamReadException, DatabindException, IOException {
		HttpRequest request = HttpRequest.get(baseUrl + STATUS_URL);
		return process(request, StatusResponse.class);

	}
	
	private <T> T process(HttpRequest request, Class<T> type) throws OkxCommunicationException, OkxRequestException  {
		log.debug(request.toString());
		HttpResponse response = request.send();
		if(response.statusCode() != 200) {
			log.warn(request.queryString() + "\n" + response.toString());
			throw new OkxCommunicationException(request.queryString() + " returned code: " + response.statusCode());
		} 
		log.debug(response.toString());
		try {
			return mapper.readValue(response.bodyBytes(), type);
		} catch (IOException e) {
			log.error("Error parsing body: ", e);
			log.warn("Unable to parse response body: \n" + response.bodyText());
			throw new OkxRequestException("Unable to parse response body.", e);
		}
		
	}
}
