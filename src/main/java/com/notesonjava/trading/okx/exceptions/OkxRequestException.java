package com.notesonjava.trading.okx.exceptions;

import java.io.IOException;

public class OkxRequestException extends IOException {

	public OkxRequestException() {
		super();
	}
	
	public OkxRequestException(String message, Throwable cause) {
		super(message, cause);
	}

	public OkxRequestException(String message) {
		super(message);
	}

	public OkxRequestException(Throwable cause) {
		super(cause);
	}

}
