package com.notesonjava.trading.okx.exceptions;

import java.io.IOException;

public class OkxCommunicationException extends IOException {

	public OkxCommunicationException() {
		super();
	}
	
	public OkxCommunicationException(String message, Throwable cause) {
		super(message, cause);
	}

	public OkxCommunicationException(String message) {
		super(message);
	}

	public OkxCommunicationException(Throwable cause) {
		super(cause);
	}

}
