package com.notesonjava.trading.okx;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;

import javax.naming.CommunicationException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.notesonjava.trading.okx.exceptions.OkxCommunicationException;
import com.notesonjava.trading.okx.exceptions.OkxRequestException;

import jodd.http.HttpException;
import jodd.http.HttpRequest;
import jodd.http.HttpResponse;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@AllArgsConstructor
public class SecureRequestProcessor {
	
	private String baseUrl = "https://www.okx.com";
	private HttpHeaderHelper helper;
	private ObjectMapper mapper;
	
	private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
	private static final List<Integer> BAD_REQUEST_CODES = Arrays.asList(400, 401, 403);
	
	
	public <T> T processPost(String url, String jsonBody, Class<T> type) throws OkxCommunicationException, OkxRequestException {
		String timestamp = ZonedDateTime.now(ZoneOffset.UTC).format(formatter);
		HttpRequest request = HttpRequest.post(baseUrl + url)
				.header(helper.createHeaders(timestamp, "POST", url, jsonBody))
				.contentType("application/json").body(jsonBody);
		return processResponse(type, request); 

	}

	public <T> T processGet(String requestUrl, Class<T> type) throws OkxCommunicationException, OkxRequestException {
		String timestamp = ZonedDateTime.now(ZoneOffset.UTC).format(formatter);
		HttpRequest request = HttpRequest.get(baseUrl + requestUrl)
				.header(helper.createHeaders(timestamp, "GET", requestUrl)).timeout(1000);
		return processResponse(type, request); 
		
		
	}

	private <T> T processResponse(Class<T> type, HttpRequest request)
			throws OkxRequestException, OkxCommunicationException {
		log.debug(request.toString());
		HttpResponse response = null;
		try {
			response = request.timeout(1000).send();
		} catch (HttpException e) {
			throw new OkxCommunicationException("Request Timeout", e);
		}
		if (response.statusCode() != 200) {
			if(BAD_REQUEST_CODES.contains(response.statusCode())) {
				throw new OkxRequestException("Bad Request: " + response.bodyText());
			}
			
			log.warn(request.url() + "\n" + response.toString());
			throw new OkxCommunicationException(request.queryString() + " returned code: " + response.statusCode());
		}
		try {
			log.debug(response.toString());
			return mapper.readValue(response.bodyBytes(), type);
		}  catch (IOException e) {
			log.warn("Unable to parse response body: \n" + response.bodyText());
			throw new OkxRequestException("Unable to parse response body.");
		}
	}
	
}
