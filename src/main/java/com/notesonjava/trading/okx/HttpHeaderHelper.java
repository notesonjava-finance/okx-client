package com.notesonjava.trading.okx;

import java.util.HashMap;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;


@RequiredArgsConstructor
@AllArgsConstructor
class HttpHeaderHelper {

	@NonNull
	private String apiKey = "daf450ce-ce0f-4097-9c26-816deabf9cad";
	@NonNull
	private String passPhrase = "RfpcSnX1K7fqDlwl4Lan!";
	@NonNull
	private SignatureHelper sigHelper;
	
	private boolean demo = false;

	
	
	public Map<String, String> createHeaders(String timestamp, String method, String requestPath) {
		String signature = sigHelper.createSignature(timestamp, method, requestPath);

		Map<String, String> headers = new HashMap<>();
		headers.put("OK-ACCESS-KEY", apiKey);
		headers.put("OK-ACCESS-SIGN", signature);
		headers.put("OK-ACCESS-TIMESTAMP", timestamp);
		headers.put("OK-ACCESS-PASSPHRASE", passPhrase);
		if(demo) {
			headers.put("x-simulated-trading", "1");
		}
		return headers;
	}

	public Map<String, String> createHeaders(String timestamp, String method, String requestPath, String body) {
		String signature = sigHelper.createSignature(timestamp, method, requestPath, body);

		Map<String, String> headers = new HashMap<>();
		headers.put("OK-ACCESS-KEY", apiKey);
		headers.put("OK-ACCESS-SIGN", signature);
		headers.put("OK-ACCESS-TIMESTAMP", timestamp);
		headers.put("OK-ACCESS-PASSPHRASE", passPhrase);
		if(demo) {
			headers.put("x-simulated-trading", "1");	
		}
		return headers;
	}


}
