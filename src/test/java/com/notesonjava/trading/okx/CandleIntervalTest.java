package com.notesonjava.trading.okx;

import static org.junit.jupiter.api.Assertions.assertThrows;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import com.notesonjava.trading.okx.dto.CandleInterval;

public class CandleIntervalTest {
	
	@Test
	public void fromCode_minute() {
		CandleInterval result = CandleInterval.fromCode("1m");
		Assertions.assertThat(result).isEqualTo(CandleInterval.MINUTE_1);
	}
	
	@Test
	public void fromCode_hour() {
		CandleInterval result = CandleInterval.fromCode("1H");
		Assertions.assertThat(result).isEqualTo(CandleInterval.HOUR_1);
	}
	
	@Test
	public void fromCode_day() {
		CandleInterval result = CandleInterval.fromCode("1D");
		Assertions.assertThat(result).isEqualTo(CandleInterval.DAY_1);
	}

	@Test
	public void fromCode_week() {
		CandleInterval result = CandleInterval.fromCode("1W");
		Assertions.assertThat(result).isEqualTo(CandleInterval.WEEK_1);
	}
	
	@Test
	public void fromCode_month() {
		CandleInterval result = CandleInterval.fromCode("1M");
		Assertions.assertThat(result).isEqualTo(CandleInterval.MONTH_1);
	}
	
	@Test
	public void fromCode_invalid() {
		assertThrows(IllegalArgumentException.class, () -> {
			CandleInterval result = CandleInterval.fromCode("1S");
		});
	}
	
	
	

}
