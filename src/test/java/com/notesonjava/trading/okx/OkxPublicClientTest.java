package com.notesonjava.trading.okx;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import org.apache.commons.io.IOUtils;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.exc.StreamReadException;
import com.fasterxml.jackson.databind.DatabindException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.http.Fault;
import com.github.tomakehurst.wiremock.junit5.WireMockRuntimeInfo;
import com.github.tomakehurst.wiremock.junit5.WireMockTest;
import com.notesonjava.trading.okx.dto.CandleInterval;
import com.notesonjava.trading.okx.dto.CandleResponse;
import com.notesonjava.trading.okx.dto.OkxCandle;
import com.notesonjava.trading.okx.dto.OkxTicker;
import com.notesonjava.trading.okx.dto.TickerResponse;
import com.notesonjava.trading.okx.exceptions.OkxCommunicationException;
import com.notesonjava.trading.okx.exceptions.OkxRequestException;

@WireMockTest(httpsEnabled = true)
public class OkxPublicClientTest {

	private static final String SYMBOL = "BTC-USDC";

	@Test
	public void ticker(WireMockRuntimeInfo wmRuntimeInfo) throws IOException {

		WireMock.stubFor(WireMock.get(OkxPublicClient.TICKER_URL + "?instId=BTC-USDC")
				.willReturn(WireMock.ok().withBodyFile("ticker.json")));

		String baseUrl = wmRuntimeInfo.getHttpBaseUrl();

		OkxPublicClient client = createClient(baseUrl);
		TickerResponse response = client.getTicker(SYMBOL);

		Assertions.assertThat(response.getData()).hasSize(1);
		OkxTicker ticker = response.getData().get(0);
		Assertions.assertThat(ticker.getInstId()).isEqualTo(SYMBOL);
		Assertions.assertThat(ticker.getInstType()).isEqualTo("SPOT");
		Assertions.assertThat(ticker.getAskPx()).isEqualTo(22854.3);
		Assertions.assertThat(ticker.getBidPx()).isEqualTo(22854.2);
	}

	@Test
	public void ticker_malformed(WireMockRuntimeInfo wmRuntimeInfo)
			throws StreamReadException, DatabindException, IOException {

		WireMock.stubFor(WireMock.get(OkxPublicClient.TICKER_URL + "?instId=BTC-USDC")
				.willReturn(WireMock.ok().withFault(Fault.MALFORMED_RESPONSE_CHUNK)));
		String baseUrl = wmRuntimeInfo.getHttpBaseUrl();
		OkxPublicClient client = createClient(baseUrl);

		Assertions.assertThatThrownBy(() -> {
			client.getTicker(SYMBOL);
		}).isInstanceOf(OkxRequestException.class);
	}

	@Test
	public void ticker_garbage(WireMockRuntimeInfo wmRuntimeInfo)
			throws StreamReadException, DatabindException, IOException {

		WireMock.stubFor(WireMock.get(OkxPublicClient.TICKER_URL + "?instId=BTC-USDC")
				.willReturn(WireMock.ok().withFault(Fault.RANDOM_DATA_THEN_CLOSE)));

		String baseUrl = wmRuntimeInfo.getHttpBaseUrl();

		OkxPublicClient client = createClient(baseUrl);
		Assertions.assertThatThrownBy(() -> {
			client.getTicker(SYMBOL);
		}).isInstanceOf(OkxCommunicationException.class);

	}

	@Test
	public void ticker_connection_reset(WireMockRuntimeInfo wmRuntimeInfo)
			throws StreamReadException, DatabindException, IOException {

		WireMock.stubFor(WireMock.get(OkxPublicClient.TICKER_URL + "?instId=BTC-USDC")
				.willReturn(WireMock.ok().withFixedDelay(2000)));

		String baseUrl = wmRuntimeInfo.getHttpBaseUrl();

		OkxPublicClient client = createClient(baseUrl);

		Assertions.assertThatThrownBy(() -> {
			client.getTicker(SYMBOL);
		}).isInstanceOf(OkxRequestException.class);

	}

	@Test
	public void candles(WireMockRuntimeInfo wmRuntimeInfo) throws IOException {

		WireMock.stubFor(WireMock.get(OkxPublicClient.CANDLE_URL + "?instId=BTC-USDC" + "&limit=20&bar=4H")
				.willReturn(WireMock.ok().withBodyFile("candles.json")));

		String baseUrl = wmRuntimeInfo.getHttpBaseUrl();

		OkxPublicClient client = createClient(baseUrl);
		CandleResponse response = client.getCandles(SYMBOL, CandleInterval.HOUR_4);

		Assertions.assertThat(response.getCode()).isEqualTo(0);

		OkxCandle current = response.getData().get(0);
		Assertions.assertThat(current.getClose()).isEqualTo(22883);
		Assertions.assertThat(current.getOpen()).isEqualTo(22818.6);
		Assertions.assertThat(current.getHigh()).isEqualTo(22951.5);
		Assertions.assertThat(current.getLow()).isEqualTo(22753.7);
		Assertions.assertThat(current.getVolume()).isEqualTo(144.67057424);
		Assertions.assertThat(current.getOpenTime()).isEqualTo(1676476800000L);
		Assertions.assertThat(current.getCloseTime()).isEqualTo(0L);
		Assertions.assertThat(current.isCompleted()).isFalse();

		OkxCandle closed = response.getData().get(1);
		Assertions.assertThat(closed.getClose()).isEqualTo(22818.6);
		Assertions.assertThat(closed.getOpen()).isEqualTo(22456.3);
		Assertions.assertThat(closed.getHigh()).isEqualTo(22972.6);
		Assertions.assertThat(closed.getLow()).isEqualTo(22456.3);
		Assertions.assertThat(closed.getVolume()).isEqualTo(433.81976314);
		Assertions.assertThat(closed.getOpenTime()).isEqualTo(1676462400000L);
		Assertions.assertThat(closed.getCloseTime()).isEqualTo(0L);
		Assertions.assertThat(closed.isCompleted()).isTrue();

	}

	@Test
	public void failed_candles(WireMockRuntimeInfo wmRuntimeInfo) throws IOException {
		WireMock.stubFor(WireMock.get(OkxPublicClient.CANDLE_URL + "?instId=BTC-USDC" + "&limit=20&bar=4H")
				.willReturn(WireMock.ok().withBodyFile("failed_candles.json")));

		String baseUrl = wmRuntimeInfo.getHttpBaseUrl();

		OkxPublicClient client = createClient(baseUrl);
		CandleResponse response = client.getCandles(SYMBOL, CandleInterval.HOUR_4);

		Assertions.assertThat(response.getCode()).isEqualTo(0);

	}

	private OkxPublicClient createClient(String baseUrl) {
		ObjectMapper mapper = OkxClientFactory.createObjectMapper();
		return new OkxPublicClient(baseUrl, mapper);
	}

}
