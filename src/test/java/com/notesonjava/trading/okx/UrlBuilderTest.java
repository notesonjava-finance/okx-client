package com.notesonjava.trading.okx;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

public class UrlBuilderTest {
	//api/v5/account/balance?ccy=BTC,ETH
	@Test
	public void balances() {
		String result = UrlBuilder.getBalances();
		Assertions.assertThat(result).isEqualTo(UrlBuilder.BALANCE_URL);
	}

	@Test
	public void balances_single() {
		String result = UrlBuilder.getBalances("BTC");
		Assertions.assertThat(result).isEqualTo(UrlBuilder.BALANCE_URL+"?ccy=BTC");
	}
	
	@Test
	public void balances_multi() {
		String result = UrlBuilder.getBalances("BTC", "ETH");
		Assertions.assertThat(result).isEqualTo(UrlBuilder.BALANCE_URL+"?ccy=BTC,ETH");
	}
	
	@Test
	public void orderDetails() {
		String result = UrlBuilder.getOrderDetails("1234", "ETH");
		Assertions.assertThat(result).isEqualTo(UrlBuilder.ORDER_DETAIL_URL+"?ordId=1234&instId=ETH");
	}
	
	@Test
	public void orderHistory() {
		String result = UrlBuilder.getOrderHistory("BTC");
		Assertions.assertThat(result).isEqualTo(UrlBuilder.ORDER_HISTORY_URL+"?instType=SPOT&instId=BTC");
	}
	
	@Test
	public void orderHistoryFrom() {
		String result = UrlBuilder.getOrderHistory("BTC", 6000);
		Assertions.assertThat(result).isEqualTo(UrlBuilder.ORDER_HISTORY_URL+"?instType=SPOT&instId=BTC&begin=6000");
	}
}
