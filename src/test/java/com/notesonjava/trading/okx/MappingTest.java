package com.notesonjava.trading.okx;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.notesonjava.trading.okx.dto.StatusResponse;
import com.notesonjava.trading.okx.dto.secure.AmendOrderResponse;
import com.notesonjava.trading.okx.dto.secure.BalanceResponse;
import com.notesonjava.trading.okx.dto.secure.CancelOrderResponse;
import com.notesonjava.trading.okx.dto.secure.OrderDetailsResponse;
import com.notesonjava.trading.okx.dto.secure.OrderHistoryResponse;
import com.notesonjava.trading.okx.dto.secure.OrderListResponse;
import com.notesonjava.trading.okx.dto.secure.SubmitOrderResponse;
import com.notesonjava.trading.okx.dto.secure.model.Balance;
import com.notesonjava.trading.okx.dto.secure.model.BalanceDetails;
import com.notesonjava.trading.okx.dto.secure.model.CancelledOrder;
import com.notesonjava.trading.okx.dto.secure.model.OrderDetails;
import com.notesonjava.trading.okx.dto.secure.model.SubmittedOrder;
import com.notesonjava.trading.okx.dto.secure.types.InstrumentType;
import com.notesonjava.trading.okx.dto.secure.types.OrderSide;
import com.notesonjava.trading.okx.dto.secure.types.OrderState;
import com.notesonjava.trading.okx.dto.secure.types.OrderType;
import com.notesonjava.trading.okx.dto.secure.types.TradeMode;

public class MappingTest {
	
	private ObjectMapper mapper;
	private static final String SYMBOL = "BTC-USDC";

	
	@BeforeEach
	public void setup() {
		mapper = OkxClientFactory.createObjectMapper();
	}
	
	@Test
	public void amendOrder() throws IOException {
		try(InputStream is = this.getClass().getClassLoader().getResourceAsStream("__files/amended.json")) { 
			String content = IOUtils.toString(is, StandardCharsets.UTF_8);
			AmendOrderResponse response = mapper.readValue(content, AmendOrderResponse.class);
			assertThat(response.getCode()).isEqualTo(0);
			assertThat(response.getMessage()).isBlank();
			assertThat(response.getData()).hasSize(1);		
		}
	}
	
	@Test
	public void amendOrders() throws IOException {
		try(InputStream is = this.getClass().getClassLoader().getResourceAsStream("__files/amendedOrders.json")) { 
			String content = IOUtils.toString(is, StandardCharsets.UTF_8);
			AmendOrderResponse response = mapper.readValue(content, AmendOrderResponse.class);
			assertThat(response.getCode()).isEqualTo(0);
			assertThat(response.getMessage()).isBlank();
			assertThat(response.getData()).hasSize(2);		
		}
	}
	
	
	@Test
	public void cancelOrder() throws IOException {
		try(InputStream is = this.getClass().getClassLoader().getResourceAsStream("__files/cancel.json")) { 
			String content = IOUtils.toString(is, StandardCharsets.UTF_8);
			CancelOrderResponse response = mapper.readValue(content, CancelOrderResponse.class);
			assertThat(response.getCode()).isEqualTo(0);
			assertThat(response.getMessage()).isBlank();
			assertThat(response.getData()).hasSize(1);		
		}
	}
	
	@Test
	public void cancelOrders() throws IOException {
		try(InputStream is = this.getClass().getClassLoader().getResourceAsStream("__files/cancelOrders.json")) { 
			String content = IOUtils.toString(is, StandardCharsets.UTF_8);
			CancelOrderResponse response = mapper.readValue(content, CancelOrderResponse.class);
			assertThat(response.getMessage()).isBlank();
			assertThat(response.getData()).hasSize(2);
			
			CancelledOrder o1 = response.getData().get(0);
			assertThat(o1.getClientId()).isEqualTo("oktswap6");
			assertThat(o1.getCode()).isEqualTo(0);
			assertThat(o1.getMessage()).isBlank();
			assertThat(o1.getServerId()).isEqualTo("12345689");
			
			CancelledOrder o2 = response.getData().get(1);
			assertThat(o2.getClientId()).isEqualTo("oktswap7");
			assertThat(o2.getCode()).isEqualTo(0);
			assertThat(o2.getMessage()).isBlank();
			assertThat(o2.getServerId()).isEqualTo("12344");		
		}
	}
	
	@Test
	public void submitOrder() throws IOException {
		try(InputStream is = this.getClass().getClassLoader().getResourceAsStream("__files/submit.json")) { 
			String content = IOUtils.toString(is, StandardCharsets.UTF_8);
			SubmitOrderResponse response = mapper.readValue(content, SubmitOrderResponse.class);
			assertThat(response.getCode()).isEqualTo(0);
			assertThat(response.getMessage()).isBlank();
			assertThat(response.getData()).hasSize(1);		
		}
	}
	
	@Test
	public void details() throws IOException {
		try(InputStream is = this.getClass().getClassLoader().getResourceAsStream("__files/details.json")) { 
			String content = IOUtils.toString(is, StandardCharsets.UTF_8);
			OrderListResponse response = mapper.readValue(content, OrderListResponse.class);
			assertThat(response.getCode()).isEqualTo(0);
			assertThat(response.getMessage()).isBlank();
			assertThat(response.getData()).hasSize(1);		
		}
	}
	
	@Test
	public void list() throws IOException {
		try(InputStream is = this.getClass().getClassLoader().getResourceAsStream("__files/list.json")) { 
			String content = IOUtils.toString(is, StandardCharsets.UTF_8);
			OrderDetailsResponse response = mapper.readValue(content, OrderDetailsResponse.class);
			assertThat(response.getCode()).isEqualTo(0);
			assertThat(response.getMessage()).isBlank();
			assertThat(response.getData()).hasSize(1);		
		}
	}
	
	
	
	@Test
	public void submitOrders() throws IOException {
		try(InputStream is = this.getClass().getClassLoader().getResourceAsStream("__files/submitOrders.json")) { 
			String content = IOUtils.toString(is, StandardCharsets.UTF_8);
			SubmitOrderResponse response = mapper.readValue(content, SubmitOrderResponse.class);
			assertThat(response.getMessage()).isBlank();
			assertThat(response.getData()).hasSize(2);
			
			SubmittedOrder o1 = response.getData().get(0);
			assertThat(o1.getClientId()).isEqualTo("oktswap6");
			assertThat(o1.getCode()).isEqualTo(0);
			assertThat(o1.getMessage()).isBlank();
			assertThat(o1.getServerId()).isEqualTo("12345689");
			
			SubmittedOrder o2 = response.getData().get(1);
			assertThat(o2.getClientId()).isEqualTo("oktswap7");
			assertThat(o2.getCode()).isEqualTo(0);
			assertThat(o2.getMessage()).isBlank();
			assertThat(o2.getServerId()).isEqualTo("12344");		
		}
	}	
	
	
	
	
	
	
	@Test
	public void orderHistory() throws IOException {
		try(InputStream is = this.getClass().getClassLoader().getResourceAsStream("__files/cancelledOrdersHistory.json")){ 
			String content = IOUtils.toString(is, StandardCharsets.UTF_8);
			OrderHistoryResponse response = mapper.readValue(content, OrderHistoryResponse.class);
			
			List<OrderDetails> orders = response.getData();
			// order is cancelled by user

			assertThat(orders).hasSize(2);
			OrderDetails o = orders.get(0);
			assertThat(o.getInstrumentId()).isEqualTo(SYMBOL);
			assertThat(o.getInstrumentType()).isEqualTo(InstrumentType.SPOT);
			assertThat(o.getAccumulatedFill()).isEqualTo(0.0);
			assertThat(o.getAveragePrice()).isEqualTo(0.0);
			assertThat(o.getCreationTime()).isEqualTo(1681421372415L);
			assertThat(o.getFee()).isEqualTo(0.0);
			assertThat(o.getFillPrice()).isEqualTo(0.0);

			assertThat(o.getFillSize()).isEqualTo(0.0);
			assertThat(o.getFillTime()).isEqualTo(0L);
			assertThat(o.getPrice()).isEqualTo(21121);
			assertThat(o.getSize()).isEqualTo(0.02);
			assertThat(o.getUpdateTime()).isEqualTo(1681428207173L);
			assertThat(o.getClientId()).isEqualTo("");
			assertThat(o.getFeeCurrency()).isEqualTo("BTC");
			assertThat(o.getInstrumentId()).isEqualTo("BTC-USDC");
			assertThat(o.getPositionSide()).isEqualTo("");
			assertThat(o.getRebate()).isEqualTo("0");
			assertThat(o.getRebateCurrency()).isEqualTo("USDC");
			assertThat(o.getServerId()).isEqualTo("566857553318105089");
			assertThat(o.getSource()).isEqualTo("");
			assertThat(o.getTag()).isEqualTo("");
			assertThat(o.getTargetCurrency()).isEqualTo("");
			assertThat(o.getTradeId()).isEqualTo("");
			assertThat(o.getInstrumentType()).isEqualTo(InstrumentType.SPOT);
			assertThat(o.getOrderType()).isEqualTo(OrderType.LIMIT);
			assertThat(o.getSide()).isEqualTo(OrderSide.BUY);
			assertThat(o.getTradeMode()).isEqualTo(TradeMode.CASH);
			assertThat(o.getState()).isEqualTo(OrderState.CANCELED);
		}
	}

	@Test
	public void status() throws IOException {
		try(InputStream is = this.getClass().getClassLoader().getResourceAsStream("__files/status.json")) { 
			String content = IOUtils.toString(is, StandardCharsets.UTF_8);
			StatusResponse response = mapper.readValue(content, StatusResponse.class);
			assertThat(response.getCode()).isEqualTo(0);
			assertThat(response.getMessage()).isBlank();
			assertThat(response.getData()).hasSize(1);		
		}
	}
	
	@Test
	public void balance() throws IOException {
		try(InputStream is = this.getClass().getClassLoader().getResourceAsStream("__files/balance.json")) { 
			String content = IOUtils.toString(is, StandardCharsets.UTF_8);
			BalanceResponse response = mapper.readValue(content, BalanceResponse.class);
			assertThat(response.getCode()).isEqualTo(0);
			assertThat(response.getMessage()).isBlank();
			assertThat(response.getData()).hasSize(1);
			Balance balance = response.getData().get(0);
			assertThat(balance.getDetails()).hasSize(2);
			BalanceDetails details = balance.getDetails().get(0);
			assertThat(details.getCurrency()).isEqualTo("USDT");
		}
	}
	
	
}
