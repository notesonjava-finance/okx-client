package com.notesonjava.trading.okx;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class HttpHeaderHelperTest {
	
	private HttpHeaderHelper httpHelper;
	private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
	
	
	@BeforeEach
	public void setup() {
		SignatureHelper sigHelper = new SignatureHelper("secret");
		httpHelper = new HttpHeaderHelper("key", "pass", sigHelper, false);
	}
	
	@Test
	public void createHeaders_get() {
		String timestamp = ZonedDateTime.of(2023, 2, 2, 13, 40, 8, 123, ZoneOffset.UTC).format(formatter);
		Map<String, String> headers = httpHelper.createHeaders(timestamp, "GET", OkxSecureClient.ORDER_LIST_URL);
		
		assertThat(headers.size()).isEqualTo(4);
	
		assertThat(headers.get("OK-ACCESS-KEY")).isEqualTo("key");
		assertThat(headers.get("OK-ACCESS-SIGN")).isEqualTo("E4TdXi+lns6M3yOBS8uAb9vHCP7/HBymh21vihZwU/0=");
		assertThat(headers.get("OK-ACCESS-TIMESTAMP")).isEqualTo(timestamp);
		assertThat(headers.get("OK-ACCESS-PASSPHRASE")).isEqualTo("pass");
		assertThat(headers).doesNotContainKey("x-simulated-trading");
	}

	@Test
	public void createHeaders_post() {
		String timestamp = ZonedDateTime.of(2023, 2, 2, 13, 40, 8, 123, ZoneOffset.UTC).format(formatter);
		
		Map<String, String> headers = httpHelper.createHeaders(timestamp, "POST", OkxSecureClient.ORDER_LIST_URL, "body");
		
		assertThat(headers.size()).isEqualTo(4);
	
		assertThat(headers.get("OK-ACCESS-KEY")).isEqualTo("key");
		assertThat(headers.get("OK-ACCESS-SIGN")).isEqualTo("E566UOrzEHSvsk/5Q/MhRtJxi38kw3lYRbVUkBW0BeI=");
		assertThat(headers.get("OK-ACCESS-TIMESTAMP")).isEqualTo(timestamp);
		assertThat(headers.get("OK-ACCESS-PASSPHRASE")).isEqualTo("pass");
		assertThat(headers).doesNotContainKey("x-simulated-trading");
	}

}
