package com.notesonjava.trading.okx;

import static org.assertj.core.api.Assertions.assertThat;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.http.Fault;
import com.github.tomakehurst.wiremock.junit5.WireMockRuntimeInfo;
import com.github.tomakehurst.wiremock.junit5.WireMockTest;
import com.notesonjava.trading.okx.dto.secure.OrderHistoryResponse;
import com.notesonjava.trading.okx.exceptions.OkxCommunicationException;
import com.notesonjava.trading.okx.exceptions.OkxRequestException;

@WireMockTest(httpsEnabled = true)
public class SecureRequestProcessorTest {

	private SecureRequestProcessor processor;
	private SignatureHelper sigHelper;
	private HttpHeaderHelper httpHelper;
	private ObjectMapper mapper;

	@BeforeEach
	void setUp() {
		sigHelper = new SignatureHelper("secret");
		httpHelper = new HttpHeaderHelper(sigHelper);
		mapper = new ObjectMapper();
	}

	@Test
	public void processGet(WireMockRuntimeInfo wmRuntimeInfo) throws OkxCommunicationException, OkxRequestException {
		String baseUrl = wmRuntimeInfo.getHttpBaseUrl();
		processor = new SecureRequestProcessor(baseUrl, httpHelper, mapper);

		String url = UrlBuilder.getOrderHistory("BTC-USDC");
		WireMock.stubFor(WireMock.get(url).willReturn(WireMock.ok().withBodyFile("cancelledOrdersHistory.json")));

		OrderHistoryResponse response = processor.processGet(url, OrderHistoryResponse.class);
		assertThat(response.getCode()).isEqualTo(0);
	}

	@Test
	public void processGet_unauthorized(WireMockRuntimeInfo wmRuntimeInfo) throws OkxCommunicationException, OkxRequestException {
		String baseUrl = wmRuntimeInfo.getHttpBaseUrl();
		processor = new SecureRequestProcessor(baseUrl, httpHelper, mapper);

		String url = UrlBuilder.getOrderHistory("BTC-USDC");
		WireMock.stubFor(WireMock.get(url).willReturn(WireMock.aResponse().withStatus(401)));
		
		Assertions.assertThatThrownBy(() -> {
			processor.processGet(url, OrderHistoryResponse.class);
		}).isInstanceOf(OkxRequestException.class);
	
	}
	
	@Test
	public void processGet_badRequest(WireMockRuntimeInfo wmRuntimeInfo) throws OkxCommunicationException, OkxRequestException {
		String baseUrl = wmRuntimeInfo.getHttpBaseUrl();
		processor = new SecureRequestProcessor(baseUrl, httpHelper, mapper);

		String url = UrlBuilder.getOrderHistory("BTC-USDC");
		WireMock.stubFor(WireMock.get(url).willReturn(WireMock.aResponse().withStatus(400)));
		
		Assertions.assertThatThrownBy(() -> {
			processor.processGet(url, OrderHistoryResponse.class);
		}).isInstanceOf(OkxRequestException.class);
	
	}

	@Test
	public void processGet_forbidden(WireMockRuntimeInfo wmRuntimeInfo) throws OkxCommunicationException, OkxRequestException {
		String baseUrl = wmRuntimeInfo.getHttpBaseUrl();
		processor = new SecureRequestProcessor(baseUrl, httpHelper, mapper);

		String url = UrlBuilder.getOrderHistory("BTC-USDC");
		WireMock.stubFor(WireMock.get(url).willReturn(WireMock.aResponse().withStatus(400)));
		
		Assertions.assertThatThrownBy(() -> {
			processor.processGet(url, OrderHistoryResponse.class);
		}).isInstanceOf(OkxRequestException.class);
	
	}

	@Test
	public void processGet_timeout(WireMockRuntimeInfo wmRuntimeInfo) throws OkxCommunicationException, OkxRequestException {
		String baseUrl = wmRuntimeInfo.getHttpBaseUrl();
		processor = new SecureRequestProcessor(baseUrl, httpHelper, mapper);

		String url = UrlBuilder.getOrderHistory("BTC-USDC");
		WireMock.stubFor(WireMock.get(url).willReturn(WireMock.ok().withFixedDelay(1500)));
		
		Assertions.assertThatThrownBy(() -> {
			processor.processGet(url, OrderHistoryResponse.class);
		}).isInstanceOf(OkxCommunicationException.class);
	
	}

	@Test
	public void processGet_server_error(WireMockRuntimeInfo wmRuntimeInfo) throws OkxCommunicationException, OkxRequestException {
		String baseUrl = wmRuntimeInfo.getHttpBaseUrl();
		processor = new SecureRequestProcessor(baseUrl, httpHelper, mapper);

		String url = UrlBuilder.getOrderHistory("BTC-USDC");
		WireMock.stubFor(WireMock.get(url).willReturn(WireMock.aResponse().withStatus(500)));
		
		Assertions.assertThatThrownBy(() -> {
			processor.processGet(url, OrderHistoryResponse.class);
		}).isInstanceOf(OkxCommunicationException.class);
	
	}


	@Test
	public void processGet_connectionReset(WireMockRuntimeInfo wmRuntimeInfo) throws OkxCommunicationException, OkxRequestException {
		String baseUrl = wmRuntimeInfo.getHttpBaseUrl();
		processor = new SecureRequestProcessor(baseUrl, httpHelper, mapper);

		String url = UrlBuilder.getOrderHistory("BTC-USDC");
		WireMock.stubFor(WireMock.get(url).willReturn(WireMock.aResponse().withFault(Fault.CONNECTION_RESET_BY_PEER)));
		
		Assertions.assertThatThrownBy(() -> {
			processor.processGet(url, OrderHistoryResponse.class);
		}).isInstanceOf(OkxCommunicationException.class);
	
	}
	
	@Test
	public void processGet_malformedChunk(WireMockRuntimeInfo wmRuntimeInfo) throws OkxCommunicationException, OkxRequestException {
		String baseUrl = wmRuntimeInfo.getHttpBaseUrl();
		processor = new SecureRequestProcessor(baseUrl, httpHelper, mapper);

		String url = UrlBuilder.getOrderHistory("BTC-USDC");
		WireMock.stubFor(WireMock.get(url).willReturn(WireMock.aResponse().withFault(Fault.MALFORMED_RESPONSE_CHUNK)));
		
		Assertions.assertThatThrownBy(() -> {
			processor.processGet(url, OrderHistoryResponse.class);
		}).isInstanceOf(OkxRequestException.class);
	}
	
	@Test
	public void processGet_garbage(WireMockRuntimeInfo wmRuntimeInfo) throws OkxCommunicationException, OkxRequestException {
		String baseUrl = wmRuntimeInfo.getHttpBaseUrl();
		processor = new SecureRequestProcessor(baseUrl, httpHelper, mapper);

		String url = UrlBuilder.getOrderHistory("BTC-USDC");
		WireMock.stubFor(WireMock.get(url).willReturn(WireMock.ok().withFault(Fault.RANDOM_DATA_THEN_CLOSE)));
		
		Assertions.assertThatThrownBy(() -> {
			processor.processGet(url, OrderHistoryResponse.class);
		}).isInstanceOf(OkxCommunicationException.class);
	}

	
	@Test
	public void processPost(WireMockRuntimeInfo wmRuntimeInfo) throws OkxCommunicationException, OkxRequestException {
		String baseUrl = wmRuntimeInfo.getHttpBaseUrl();
		processor = new SecureRequestProcessor(baseUrl, httpHelper, mapper);

		String url = UrlBuilder.ORDER_HISTORY_URL;
		WireMock.stubFor(WireMock.post(url).willReturn(WireMock.ok().withBodyFile("cancelledOrdersHistory.json")));

		String body = "body";
		
		OrderHistoryResponse response = processor.processPost(url, body, OrderHistoryResponse.class);
		assertThat(response.getCode()).isEqualTo(0);
	}

	
}
