package com.notesonjava.trading.okx;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.Test;

import com.notesonjava.trading.okx.dto.secure.AmendOrderRequest;
import com.notesonjava.trading.okx.dto.secure.CancelOrderRequest;
import com.notesonjava.trading.okx.dto.secure.SubmitOrderRequest;
import com.notesonjava.trading.okx.dto.secure.types.OrderSide;
import com.notesonjava.trading.okx.dto.secure.types.OrderType;
import com.notesonjava.trading.okx.dto.secure.types.TradeMode;

public class OrderRequestFactoryTest {

    private final OrderRequestFactory orderRequestFactory = new OrderRequestFactory();

    @Test
    void createSellOrderRequest() {
        SubmitOrderRequest request = orderRequestFactory.createSellOrderRequest(100.0, 10.0, "BTC");
        assertThat(request.getInstrumentId()).isEqualTo("BTC");
        assertThat(request.getTradeMode()).isEqualTo(TradeMode.CASH);
        assertThat(request.getSide()).isEqualTo(OrderSide.SELL);
        assertThat(request.getOrderType()).isEqualTo(OrderType.LIMIT);
        assertThat(request.getPrice()).isEqualTo(100.0);
        assertThat(request.getSize()).isEqualTo(10.0);
    }

    @Test
    void createBuyOrderRequest() {
        SubmitOrderRequest request = orderRequestFactory.createBuyOrderRequest(200.0, 20.0, "BTC");
        assertThat(request.getInstrumentId()).isEqualTo("BTC");
        assertThat(request.getTradeMode()).isEqualTo(TradeMode.CASH);
        assertThat(request.getSide()).isEqualTo(OrderSide.BUY);
        assertThat(request.getOrderType()).isEqualTo(OrderType.LIMIT);
        assertThat(request.getPrice()).isEqualTo(200.0);
        assertThat(request.getSize()).isEqualTo(20.0);
    }

    @Test
    void createAmendSizeRequest() {
        AmendOrderRequest request = orderRequestFactory.createAmendSizeRequest("order123", "BTC", 15.0);
        assertThat(request.getServerId()).isEqualTo("order123");
        assertThat(request.getSymbol()).isEqualTo("BTC");
        assertThat(request.getNewSize()).isEqualTo(15.0);
    }

    @Test
    void createAmendPriceRequest() {
        AmendOrderRequest request = orderRequestFactory.createAmendPriceRequest("order123", "BTC", 300.0);
        assertThat(request.getServerId()).isEqualTo("order123");
        assertThat(request.getSymbol()).isEqualTo("BTC");
        assertThat(request.getNewPrice()).isEqualTo(300.0);
    }

    @Test
    void createCancelRequest() {
        CancelOrderRequest request = orderRequestFactory.createCancelRequest("order123", "BTC");
        assertThat(request.getInstrumentId()).isEqualTo("BTC");
        assertThat(request.getServerId()).isEqualTo("order123");
    }

    @Test
    void createCancelOrdersRequest() {
        List<String> orderIds = Arrays.asList("order123", "order234");
        List<CancelOrderRequest> request = orderRequestFactory.createCancelOrdersRequest(orderIds, "BTC");
        assertThat(request).hasSize(2);
        assertThat(request.get(0).getInstrumentId()).isEqualTo("BTC");
        assertThat(request.get(0).getServerId()).isEqualTo("order123");
        
        assertThat(request.get(0).getInstrumentId()).isEqualTo("BTC");
        assertThat(request.get(0).getServerId()).isEqualTo("order123");
    }

    @Test
    void createAmendOrderRequest() {
        AmendOrderRequest request = orderRequestFactory.createAmendOrderRequest("order123", "BTC", 300.0, 15.0);
        assertThat(request.getServerId()).isEqualTo("order123");
        assertThat(request.getSymbol()).isEqualTo("BTC");
        assertThat(request.getNewPrice()).isEqualTo(300.0);
        assertThat(request.getNewSize()).isEqualTo(15.0);
    }
}
