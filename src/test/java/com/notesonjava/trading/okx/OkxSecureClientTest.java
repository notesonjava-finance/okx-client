package com.notesonjava.trading.okx;

import java.io.IOException;
import java.util.Arrays;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.junit5.WireMockRuntimeInfo;
import com.github.tomakehurst.wiremock.junit5.WireMockTest;
import com.notesonjava.trading.okx.dto.secure.AmendOrderRequest;
import com.notesonjava.trading.okx.dto.secure.AmendOrderResponse;
import com.notesonjava.trading.okx.dto.secure.CancelOrderRequest;
import com.notesonjava.trading.okx.dto.secure.CancelOrderResponse;
import com.notesonjava.trading.okx.dto.secure.OrderDetailsResponse;
import com.notesonjava.trading.okx.dto.secure.OrderHistoryResponse;
import com.notesonjava.trading.okx.dto.secure.OrderListResponse;
import com.notesonjava.trading.okx.dto.secure.SubmitOrderRequest;
import com.notesonjava.trading.okx.dto.secure.SubmitOrderResponse;
import com.notesonjava.trading.okx.dto.secure.types.OrderSide;
import com.notesonjava.trading.okx.dto.secure.types.OrderType;
import com.notesonjava.trading.okx.dto.secure.types.TradeMode;
import com.notesonjava.trading.okx.exceptions.OkxRequestException;

@WireMockTest(httpsEnabled = true)
public class OkxSecureClientTest {

	private static final String SYMBOL = "BTC-USDC";
	private static final String SECRET_KEY = "4986F64D535B1FDE50CC7E5D9F6CEDDE";
	private static final String API_KEY = "daf450ce-ce0f-4097-9c26-816deabf9cad";
	private static final String PASS_PHRASE = "RfpcSnX1K7fqDlwl4Lan!";

	

	@Test
	public void cancelledOrderHistory(WireMockRuntimeInfo wmRuntimeInfo) throws IOException {

			WireMock.stubFor(WireMock.get(UrlBuilder.ORDER_HISTORY_URL + "?instType=SPOT&instId=" + SYMBOL)
					.willReturn(WireMock.ok().withBodyFile("cancelledOrdersHistory.json")));

			String baseUrl = wmRuntimeInfo.getHttpBaseUrl();
			OkxSecureClient client = createClient(baseUrl);

			OrderHistoryResponse response = client.getOrderHistory(SYMBOL);
			Assertions.assertThat(response.getCode()).isEqualTo(0);
			
	}

	@Test
	public void syntaxError(WireMockRuntimeInfo wmRuntimeInfo) throws IOException {

		WireMock.stubFor(WireMock.post(OkxSecureClient.CANCEL_URL)
				.willReturn(WireMock.badRequest().withBodyFile("syntax_error.json")));

		String baseUrl = wmRuntimeInfo.getHttpBaseUrl();
		OkxSecureClient client = createClient(baseUrl);

		Assertions.assertThatThrownBy(() -> {
			client.cancelOrder("1", SYMBOL);
		}).isInstanceOf(OkxRequestException.class);
	}

	@Test
	public void cancelOrder(WireMockRuntimeInfo wmRuntimeInfo) throws IOException {
		String baseUrl = wmRuntimeInfo.getHttpBaseUrl();

		WireMock.stubFor(WireMock.post(WireMock.urlEqualTo(OkxSecureClient.CANCEL_URL))
				.willReturn(WireMock.ok().withBodyFile("cancel.json")));

		OkxSecureClient client = createClient(baseUrl);
		client.cancelOrder("1234", SYMBOL);
		CancelOrderRequest req = new CancelOrderRequest();
		req.setInstrumentId(SYMBOL);
		req.setServerId("1234");
		ObjectMapper mapper = new ObjectMapper();

		WireMock.verify(WireMock.postRequestedFor(WireMock.urlEqualTo(OkxSecureClient.CANCEL_URL))
				.withRequestBody(WireMock.equalToJson(mapper.writeValueAsString(req), true, false)));
	}

	
	@Test
	public void cancelOrders(WireMockRuntimeInfo wmRuntimeInfo) throws IOException {
		String baseUrl = wmRuntimeInfo.getHttpBaseUrl();

		WireMock.stubFor(WireMock.post(WireMock.urlEqualTo(OkxSecureClient.MULTI_CANCEL_URL))
				.willReturn(WireMock.ok().withBodyFile("cancelOrders.json")));

		OkxSecureClient client = createClient(baseUrl);
		CancelOrderResponse response = client.cancelOrders(Arrays.asList("1234", "1235", "1236"), SYMBOL);

		WireMock.verify(WireMock.postRequestedFor(WireMock.urlEqualTo(OkxSecureClient.MULTI_CANCEL_URL)));
	}

	@Test
	public void submitSellOrder(WireMockRuntimeInfo wmRuntimeInfo) throws IOException {
		String baseUrl = wmRuntimeInfo.getHttpBaseUrl();

		WireMock.stubFor(WireMock.post(WireMock.urlEqualTo(OkxSecureClient.PLACE_ORDER_URL))
				.willReturn(WireMock.ok().withBodyFile("submit.json")));

		OkxSecureClient client = createClient(baseUrl);
		SubmitOrderResponse response = client.submitSellOrder(22000, 0.01, SYMBOL);

		WireMock.verify(WireMock.postRequestedFor(WireMock.urlEqualTo(OkxSecureClient.PLACE_ORDER_URL)));
	}

	
	@Test
	public void submitBuyOrder(WireMockRuntimeInfo wmRuntimeInfo) throws IOException {
		String baseUrl = wmRuntimeInfo.getHttpBaseUrl();

		WireMock.stubFor(WireMock.post(WireMock.urlEqualTo(OkxSecureClient.PLACE_ORDER_URL))
				.willReturn(WireMock.ok().withBodyFile("submit.json")));

		OkxSecureClient client = createClient(baseUrl);
		SubmitOrderResponse response = client.submitBuyOrder(22000, 0.01, SYMBOL);

		WireMock.verify(WireMock.postRequestedFor(WireMock.urlEqualTo(OkxSecureClient.PLACE_ORDER_URL)));
	}

	@Test
	public void submitOrder(WireMockRuntimeInfo wmRuntimeInfo) throws IOException {
		String baseUrl = wmRuntimeInfo.getHttpBaseUrl();

		WireMock.stubFor(WireMock.post(WireMock.urlEqualTo(OkxSecureClient.PLACE_ORDER_URL))
				.willReturn(WireMock.ok().withBodyFile("submit.json")));

		OkxSecureClient client = createClient(baseUrl);
		SubmitOrderRequest req = new SubmitOrderRequest();
		req.setInstrumentId(SYMBOL);
		req.setOrderType(OrderType.LIMIT);
		req.setPrice(22000);
		req.setSide(OrderSide.BUY);
		req.setTradeMode(TradeMode.CASH);
		SubmitOrderResponse response = client.submitOrder(req);
		
		ObjectMapper mapper = new ObjectMapper();
		
		WireMock.verify(WireMock.postRequestedFor(WireMock.urlEqualTo(OkxSecureClient.PLACE_ORDER_URL))
				.withRequestBody(WireMock.equalToJson(mapper.writeValueAsString(req), true, false)));
	}

	@Test
	public void amendOrder(WireMockRuntimeInfo wmRuntimeInfo) throws IOException {
		String baseUrl = wmRuntimeInfo.getHttpBaseUrl();

		WireMock.stubFor(WireMock.post(WireMock.urlEqualTo(OkxSecureClient.AMEND_URL))
				.willReturn(WireMock.ok().withBodyFile("amended.json")));

		OkxSecureClient client = createClient(baseUrl);
		AmendOrderRequest req = new AmendOrderRequest();
		req.setNewPrice(20000);
		req.setNewSize(0.02);
		req.setServerId("1234");
		req.setSymbol(SYMBOL);
		
		AmendOrderResponse response = client.amendOrder(req);
		
		ObjectMapper mapper = new ObjectMapper();
		
		WireMock.verify(WireMock.postRequestedFor(WireMock.urlEqualTo(OkxSecureClient.AMEND_URL))
				.withRequestBody(WireMock.equalToJson(mapper.writeValueAsString(req), true, false)));
	}

	
	@Test
	public void orderHistory(WireMockRuntimeInfo wmRuntimeInfo) throws IOException {
		String baseUrl = wmRuntimeInfo.getHttpBaseUrl();

		String url = UrlBuilder.ORDER_HISTORY_URL + "?instType=SPOT&instId=" + SYMBOL;
		
		WireMock.stubFor(WireMock.get(WireMock.urlEqualTo(url))
				.willReturn(WireMock.ok().withBodyFile("amended.json")));

		OkxSecureClient client = createClient(baseUrl);
		
		OrderHistoryResponse response = client.getOrderHistory(SYMBOL);
		
		WireMock.verify(WireMock.getRequestedFor(WireMock.urlEqualTo(url)));
	}

	
	@Test
	public void orderDetails(WireMockRuntimeInfo wmRuntimeInfo) throws IOException {
		String baseUrl = wmRuntimeInfo.getHttpBaseUrl();
		String orderId = "1234";
		String url = UrlBuilder.ORDER_DETAIL_URL + "?ordId=" + orderId + "&instId=" + SYMBOL;

		WireMock.stubFor(WireMock.get(WireMock.urlEqualTo(url))
				.willReturn(WireMock.ok().withBodyFile("details.json")));

		OkxSecureClient client = createClient(baseUrl);
		
		OrderDetailsResponse response = client.getOrderDetails(orderId, SYMBOL);
		
		WireMock.verify(WireMock.getRequestedFor(WireMock.urlEqualTo(url)));
	}

	
	@Test
	public void activeOrders(WireMockRuntimeInfo wmRuntimeInfo) throws IOException {
		String baseUrl = wmRuntimeInfo.getHttpBaseUrl();

		WireMock.stubFor(WireMock.get(WireMock.urlEqualTo(OkxSecureClient.ORDER_LIST_URL))
				.willReturn(WireMock.ok().withBodyFile("details.json")));

		OkxSecureClient client = createClient(baseUrl);
		
		OrderListResponse response = client.getActiveOrders();
		
		WireMock.verify(WireMock.getRequestedFor(WireMock.urlEqualTo(OkxSecureClient.ORDER_LIST_URL)));
	}
	
	
	private OkxSecureClient createClient(String baseUrl) {
		OrderRequestFactory factory = new OrderRequestFactory();

		SignatureHelper sigHelper = new SignatureHelper(SECRET_KEY);
		HttpHeaderHelper httpHelper = new HttpHeaderHelper(API_KEY, PASS_PHRASE, sigHelper, true);
		ObjectMapper mapper = new ObjectMapper();
		SecureRequestProcessor processor = new SecureRequestProcessor(baseUrl, httpHelper, mapper);
		return new OkxSecureClient(mapper, factory, processor);
	}

}
